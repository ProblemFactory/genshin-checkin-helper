from config import config
# try:
#     # 优先使用httpx，在httpx无法使用的环境下使用requests
#     import httpx

#     if config['general']['global_proxy']:
#         http = httpx.Client(timeout=20, transport=httpx.HTTPTransport(retries=5), proxies=config['general']['global_proxy'])
#     else:
#         http = httpx.Client(timeout=20, transport=httpx.HTTPTransport(retries=5))
#     # 当openssl版本小于1.0.2的时候直接进行一个空请求让httpx报错
#     import tools

#     if tools.get_openssl_version() <= 102:
#         httpx.get()
# except (TypeError, ModuleNotFoundError):
#     import requests
#     from requests.adapters import HTTPAdapter

#     http = requests.Session()
#     if config['general']['global_proxy']:
#         http.proxies = {'http': config['general']['global_proxy'], 'https': config['general']['global_proxy']}
#     http.mount('http://', HTTPAdapter(max_retries=5))
#     http.mount('https://', HTTPAdapter(max_retries=5))

def get_new_session():
    try:
        # 优先使用httpx，在httpx无法使用的环境下使用requests
        import httpx
        if config['general']['global_proxy']:
            http = httpx.Client(timeout=20, transport=httpx.HTTPTransport(retries=5), proxies=config['general']['global_proxy'])
        else:
            http = httpx.Client(timeout=20, transport=httpx.HTTPTransport(retries=10))
        # 当openssl版本小于1.0.2的时候直接进行一个空请求让httpx报错
        import tools

        if tools.get_openssl_version() <= 102:
            httpx.get()
    except (TypeError, ModuleNotFoundError):
        import requests
        from requests.adapters import HTTPAdapter

        http = requests.Session()
        if config['general']['global_proxy']:
            http.proxies = {'http': config['general']['global_proxy'], 'https': config['general']['global_proxy']}
        http.mount('http://', HTTPAdapter(max_retries=10))
        http.mount('https://', HTTPAdapter(max_retries=10))
    return http


http = get_new_session()