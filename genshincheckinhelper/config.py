# """Configuration.
# @Project   : genshinhelper
# @Author    : y1ndan
# @Blog      : https://www.yindan.me
# @GitHub    : https://github.com/y1ndan
# """

# import json
# import os

# CONFIG_DICT = {
#     'LANGUAGE': 'LANGUAGE',
#     'RANDOM_SLEEP_SECS_RANGE': 'RANDOM_SLEEP_SECS_RANGE',
#     'CHECK_IN_TIME': 'CHECK_IN_TIME',
#     'CHECK_RESIN_SECS': 'CHECK_RESIN_SECS',
#     'RESIN_THRESHOLD': 'RESIN_THRESHOLD',
#     'RESIN_TIMER_DO_NOT_DISTURB': 'RESIN_TIMER_DO_NOT_DISTURB',
#     'COOKIE_MIHOYOBBS': 'COOKIE_MIHOYOBBS',
#     'COOKIE_RESIN_TIMER': 'COOKIE_RESIN_TIMER',
#     'HOME_COIN_THRESHOLD': 'HOME_COIN_THRESHOLD',
#     'RESIN_FULL_NOTIFY_COUNT': 'RESIN_FULL_NOTIFY_COUNT',
#     'HOME_COIN_FULL_NOTIFY_COUNT': 'HOME_COIN_FULL_NOTIFY_COUNT',
#     'EXPEDITION_NOTIFY_COUNT': 'EXPEDITION_NOTIFY_COUNT',
#     'TRANSFORMER_READY_NOTIFY_COUNT': 'TRANSFORMER_READY_NOTIFY_COUNT',
#     'COOKIE_BH3': 'COOKIE_BH3',
#     'COOKIE_MIYOUBI': 'COOKIE_MIYOUBI',
#     'CLOUD_GENSHIN': 'CLOUD_GENSHIN',
#     'COOKIE_HOYOLAB': 'COOKIE_HOYOLAB',
#     'COOKIE_WEIBO': 'COOKIE_WEIBO',
#     'COOKIE_KA': 'COOKIE_KA',
#     'SHOPTOKEN': 'SHOPTOKEN',
#     'ONEPUSH': 'ONEPUSH'
# }


# class Config(object):
#     """
#     Get all configuration from the config.json file.
#     If config.json dose not exist, use config.example.json file.

#         Note:   Environment variables have a higher priority,
#                 if you set a environment variable in your system,
#                 that variable in the config.json file will be invalid.
#     """

#     def __init__(self):
#         # Open and read the config file
#         # project_path = os.path.dirname(os.path.dirname(__file__))
#         project_path = os.path.dirname(__file__)
#         config_file = os.path.join(project_path, 'config', 'config.json')
#         if not os.path.exists(config_file):
#             config_file = os.path.join(project_path, 'config', 'config.example.json')

#         with open(config_file, 'r', encoding='utf-8') as f:
#             self.config_json = json.load(f)

#         for i in CONFIG_DICT:
#             self.__dict__[i] = self.get_config(i)

#     def get_config(self, key: str):
#         value = os.environ[key] if os.environ.get(key) else self.config_json.get(key, '')

#         default_config_dict = {
#             'LANGUAGE': 'en',
#             'RANDOM_SLEEP_SECS_RANGE': '0-300',
#             'CHECK_IN_TIME': '06:00',
#             'CHECK_RESIN_SECS': 900,
#             'RESIN_THRESHOLD': 150,
#             'HOME_COIN_THRESHOLD': 2360,
#             'RESIN_FULL_NOTIFY_COUNT': 5,
#             'HOME_COIN_FULL_NOTIFY_COUNT': 5,
#             'EXPEDITION_NOTIFY_COUNT': 1,
#             'TRANSFORMER_READY_NOTIFY_COUNT': 3,
#             'RESIN_TIMER_DO_NOT_DISTURB': '23:00-07:00'
#         }

#         for k, v in default_config_dict.items():
#             if key == k and not value:
#                 value = v

#         if key == 'ONEPUSH' and '{' in value:
#             value = json.loads(value)
#         return value


# config = Config()


import os
import glob
import yaml
from loghelper import log
from collections.abc import Mapping
from copy import deepcopy


def merge(dict1, dict2):
    ''' Return a new dictionary by merging two dictionaries recursively. '''

    result = deepcopy(dict1)

    for key, value in dict2.items():
        if isinstance(value, Mapping):
            result[key] = merge(result.get(key, {}), value)
        else:
            result[key] = deepcopy(dict2[key])

    return result

# 这个字段现在还没找好塞什么地方好，就先塞config这里了
serverless = False

config = {
    'general': {
        'start_time': "04:00",
        'random_sleep_secs_range': '0-100',
        'language': "zh",
        'coolxi_token': '',
        'rrocr_token': '',
        'rrocr_proxy': '',
        'global_proxy': '',
    },
    'weibo': {
        'enable': False,
        'accounts': {}
    },
    "wechat_shop": {
        "enable": False,
        "cookie": "",
        "accounts": {}
    },
    'hoyolab': {
        'accounts': {},
        'bbs': {
            'enable': False,
        },
        'games': {
            'enable': False,
        }
    },
    'mihoyo': {
        'accounts': {},
        'bbs': {
            'checkin': False, 'checkin_multi': False, 'checkin_multi_list': [1,2,3,4,5,6,7,8],
            'read_posts': False, 'like_posts': False, 'cancel_like_posts': False, 'share_post': False
        },
        'games': {
            'genshin': {
                'checkin': {'enable':False, 'black_list': [], 'birthday':True},
                'resource_check': {
                    'enable': False,
                    'do_not_disturb': "00:00-00:00",
                    'black_list': [],
                    'interval': 900,
                    'resin_threshold': 150,
                    'resin_full_notify_count': 5,
                    'home_coin_threshold': 2360,
                    'home_coin_full_notify_count': 5,
                    'expedition_notify_count': 1,
                    'transformer_ready_notify_count': 3,
                },
            },
            'starrail': {'checkin': {'enable':False, 'black_list': []}},
            'zzz': {'checkin': {'enable':False, 'black_list': []}},
            'hokai2': {'checkin': {'enable':False, 'black_list': []}},
            'honkai3rd': {'checkin': {'enable':False, 'black_list': []}},
            'tears_of_themis': {'checkin': {'enable':False, 'black_list': []}},
        },
    },
    'cloud_games': {
        "genshin": {
            'enable': False,
            'accounts': {}
        }
    },
    'notifiers': {}
}

path = os.path.dirname(os.path.realpath(__file__)) + "/config/config.yaml"

def copy_config():
    return config

def load_config(p_path=None):
    global config
    p_path = p_path or path
    with open(p_path, "r", encoding='utf-8') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    config = merge(config, data)
    log.info("配置文件加载完毕")
    return config 

# def load_configs():
#     global configs
#     files = glob.glob(os.path.join(path ,"*.yml"))+glob.glob(os.path.join(path,"*.yaml"))
#     for f in files:
#         configs[os.path.basename(f)] = load_config(f)
#     return configs


def save_config():
    global serverless
    if serverless:
        log.info("云函数执行，无法保存")
        return None
    with open(path, "w+", encoding='utf-8') as f:
        try:
            f.seek(0)
            f.truncate()
            f.write(yaml.dump(config, Dumper=yaml.Dumper, sort_keys=False))
            f.flush()
        except OSError:
            serverless = True
            log.info("Cookie保存失败")
            exit(-1)
        else:
            log.info("Config保存完毕")


load_config()
# if __name__ == "__main__":
#     # 初始化配置文件
#     # try:
#     #     account_cookie = config['account']['cookie']
#     #     config = load_config()
#     #     config['account']['cookie'] = account_cookie
#     # except OSError:
#     #     pass
#     # save_config()
#     # update_config()
#     load_configs()
#     pass