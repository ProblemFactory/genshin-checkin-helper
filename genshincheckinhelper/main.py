"""
@Project   : genshinhelper
@Author    : y1ndan
@Blog      : https://www.yindan.me
@GitHub    : https://github.com/y1ndan
"""
# pylint: disable=locally-disabled, multiple-statements, fixme, line-too-long, missing-function-docstring, invalid-name, ungrouped-imports

from collections.abc import Iterable

import datetime
import os
from time import sleep

import schedule

try:
    import genshinhelper as gh
    from config import config
except ImportError:
    import sys

    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    import genshinhelper as gh
    from genshincheckinhelper.config import config
finally:
    from genshinhelper.utils import log, nested_lookup, minutes_to_hours, MESSAGE_TEMPLATE, DAIRY_TEMPLATE, FINANCE_TEMPLATE

import signin.yuanshen
import signin.starrail
import signin.zzz
import signin.sina as sina
import signin.bbs
from signin.captcha import game_record_captcha
import setting
import account
from onepush import notify
from utils import retry_until_success, random_sleep, time_in_range, DictPersistJSON


version = '1.0.4'
banner = f'''
+----------------------------------------------------------------+
|               𒆙  Genshin Check-In Helper v{version}                |
+----------------------------------------------------------------+
Project      : genshinhelper
Description  : More than check-in for Genshin Impact.
PKG_Version  : {gh.__version__}
Author       : 银弹GCell(y1ndan)
Blog         : https://www.yindan.me
Channel      : https://t.me/genshinhelperupdates
------------------------------------------------------------------'''

def notify_single(title, content, notifier, params):
    log.info('Preparing to send notification ...')
    if type(content)==list:
        merged_content = ''
        for content_type, content_data in content:
            if content_type=='pre':
                if params.get('markdown', False):
                    merged_content += f'```\n{content_data}```'
                elif notifier in ['telegram']:
                    merged_content += f'<pre>\n{content_data}</pre>'
                else:
                    merged_content += content_data
            else:
                merged_content += content_data
        content = merged_content
    return notify(notifier, escape=True, title=title, content=content, **params)

def notify_me(title, content, notifiers=None):
    results = []
    notifiers = notifiers or config['notifiers']
    for notifier, params in notifiers.items():
        if not notifier or not params:
            log.info('No notification method configured ...')
            return
        results.append(notify_single(title, content, notifier, params))
    return results


def task_common(r, d, text_temp1, text_temp2):
    result = []
    for i in range(len(r)):
        if i>=len(d):
            continue
        if d and d[i]:
            d[i]['month'] = datetime.date.today().month
            r[i]['addons'] = text_temp2.format(**d[i])
        message = text_temp1.format(**r[i])
        result.append(message)
    return result


def genshin_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    t = gh.Genshin(account_data['cookie'])
    r = t.sign()
    d = t.month_dairy
    return task_common(r, d, MESSAGE_TEMPLATE, DAIRY_TEMPLATE)


def yuanshen_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    # t = gh.YuanShen(account_data['cookie'])
    # if 'useragent' in account_data:
    #     t.headers['User-Agent'] = account_data['useragent']
    # r = t.sign()
    # if any(['risk_code' in i['sign_response']['data'] for i in r if i['sign_response']]):
    #     for _ in range(5):
    #         d = signin.yuanshen.Genshin(account_data).sign_account()
    #         for i in range(len(r)):
    #             if r[i]['sign_response'] and 'risk_code' in r[i]['sign_response']['data']:
    #                 r[i]['status'] = d[i][0]
    #                 r[i]['total_sign_day'] = d[i][1]
    #                 if d[i][0] != 'OK':
    #                     r[i]['is_sign'] = False
    #             if d[i][0]=='OK':
    #                 break
    t = signin.yuanshen.YuanShen(account_data)
    r = t.sign_account()
    d = t.get_diary_info(required_keys={'current_primogems', 'current_mora'})
    return task_common(r, d, MESSAGE_TEMPLATE, DAIRY_TEMPLATE)

def starrail_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    t = signin.starrail.StarRail(account_data)
    r = t.sign_account()
    d = t.get_diary_info(required_keys={'current_hcoin', 'current_rails_pass'})
    return task_common(r, d, MESSAGE_TEMPLATE, 
'''开拓者的 {month} 月日志
    💎星琼: {current_hcoin}
    🎫星轨票: {current_rails_pass}''')

def zzz_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    t = signin.zzz.ZZZ(account_data)
    r = t.sign_account()
    d = t.get_diary_info(required_keys={'current_hcoin', 'current_rails_pass'})
    return task_common(r, d, MESSAGE_TEMPLATE, 
'''绳匠的 {month} 月日志
    🎞复色菲林: {current_hcoin}
    ⭐丁尼: {current_rails_pass}''')

def honkai3rd_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    t = gh.Honkai3rd(account_data['cookie'])
    if 'useragent' in account_data:
        t.headers['User-Agent'] = account_data['useragent']
    r = t.sign()
    d = t.month_finance
    return task_common(r, d, MESSAGE_TEMPLATE, FINANCE_TEMPLATE)


def miyoushe_checkin(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    if account_data.get('stoken',"")=="" or account_data.get("stuid","")=="" or account_data.get("login_ticket","")=="":
        account.login(account_data)
    t = gh.MysDailyMissions("stoken={}; stuid={}".format(account_data['stoken'],account_data['stuid']))
    if 'useragent' in account_data:
        t.headers['User-Agent'] = account_data['useragent']
    r = t.run(26)
    total_points = r['total_points']
    is_sign = r['is_sign']
    is_view = r['is_view']
    is_upvote = r['is_upvote']
    is_share = r['is_share']
    if not (is_sign and is_view and is_upvote and is_share):
        bbs = signin.bbs.Mihoyobbs(account_data)
        r = bbs.run()
        total_points = r['total_points']
        is_sign = r['is_sign']
        is_view = r['is_view']
        is_upvote = r['is_upvote']
        is_share = r['is_share']

    result_str = '''米游币: {}
    签到: {}
    浏览: {}
    点赞: {}
    分享: {}'''.format(total_points, is_sign, is_view, is_upvote, is_share)
    return [result_str]


def cloud_genshin_checkin(account_data):
    if 'token' not in account_data:
        return '请填写 token 字段 (x-rpc-combo_token)'
    cookie = account_data.get('headers')
    cookie['x-rpc-combo_token'] = account_data['token']
    r = gh.get_cloudgenshin_free_time(cookie)
    message = nested_lookup(r, 'message', fetch_first=True)
    free_time = nested_lookup(r, 'free_time', fetch_first=True)
    if not free_time:
        pass
    free_time = free_time['free_time']
    free_time_limit = nested_lookup(r, 'free_time_limit', fetch_first=True)
    total_time = nested_lookup(r, 'total_time', fetch_first=True)
    free_time_fmt = '{hour}时{minute}分'.format(**(minutes_to_hours(free_time)))
    free_time_limit_fmt = '{hour}时{minute}分'.format(
        **minutes_to_hours(free_time_limit))
    total_time_fmt = '{hour}时{minute}分'.format(**minutes_to_hours(total_time))

    result_str = '''签到结果: {}
    免费时长: {} / {}
    总计时长: {}'''.format(message, free_time_fmt, free_time_limit_fmt, total_time_fmt)
    return result_str


def weibo_checkin(account_data):
    if 'weibo_cookie' not in account_data:
        return '请填写 weibo_cookie 字段'
    t = sina.Weibo(account_data, setting.genshin_container_id)
    r = t.sign()
    log.info(r)
    result = []
    for i in r:
        lv = i['level']
        name = i['name']
        is_sign = i['is_sign']
        response = i.get('sign_response')

        status = response
        if is_sign and not response:
            status = '☑️'
        if is_sign and response:
            status = '✅'
        if not is_sign:
            status = '❌'+str(response)

        message = f'⚜️ [Lv.{lv}]{name} {status}\n    '
        result.append(message)
    return result


def task7(account_data):
    if 'ka_cookie' not in account_data:
        return '请填写 ka_cookie 字段'
    t = sina.Weibo(account_data, setting.genshin_container_id)
    is_event = t.check_event()
    if not is_event:
        return '原神超话现在没有活动哦'

    title = '原神超话签到提醒'
    content = '亲爱的旅行者, 原神微博超话签到活动现已开启, 请注意活动时间! 如已完成任务, 请忽略本信息.'
    notify_me(title, content, account_data.get('notifiers', None))
    ids = t.unclaimed_gift_ids()
    if not ids:
        recent_codes = ' *'.join(
            [f"{i['title']} {i['code']}" for i in t.get_mybox_codes()[:3]])
        msg = '原神超话签到活动已开启，但是没有未领取的兑换码。\n    最近 3 个码: '
        if account_data.get('notifiers', None):
            if notify_me(title='', content=msg+f'{recent_codes}', notifiers=account_data.get('notifiers', None)):
                return msg+'请查看私信或私人频道'
        return msg+f'{recent_codes}'

    log.info(f'检测到有 {len(ids)} 个未领取的兑换码')
    raw_codes = [t.get_code(id) for id in ids]
    msg = [('pre', str(i['code']) if i['success'] else str(i['response']['msg'])) for i in raw_codes]
    if account_data.get('notifiers', None):
        if notify_me(title='原神超话签到活动已开启，已为您领取兑换码。', content=msg, notifiers=account_data.get('notifiers', None)):
            return '原神超话签到活动已开启，已为您领取兑换码。请查看私信或私人频道'
    return [str(i['code'] + '\n    ') if i['success'] else str(i['response']['msg'] + '\n    ') for i in raw_codes]

def task7_1(account_data):
    if 'ka_cookie' not in account_data:
        return '请填写 ka_cookie 字段'
    t = sina.Weibo(account_data, setting.honkai_sr_container_id)
    is_event = t.check_event()
    if not is_event:
        return '崩坏：星穹铁道超话现在没有活动哦'

    title = '崩坏：星穹铁道超话签到提醒'
    content = '亲爱的无名客, 崩坏：星穹铁道微博超话签到活动现已开启, 请注意活动时间! 如已完成任务, 请忽略本信息.'
    notify_me(title, content, account_data.get('notifiers', None))
    ids = t.unclaimed_gift_ids()
    if not ids:
        recent_codes = ' *'.join(
            [f"{i['title']} {i['code']}" for i in t.get_mybox_codes()[:3]])
        msg = '崩坏：星穹铁道超话签到活动已开启，但是没有未领取的兑换码。\n    最近 3 个码: '
        if account_data.get('notifiers', None):
            if notify_me(title='', content=msg+f'{recent_codes}', notifiers=account_data.get('notifiers', None)):
                return msg+'请查看私信或私人频道'
        return msg+f'{recent_codes}'

    log.info(f'检测到有 {len(ids)} 个未领取的兑换码')
    raw_codes = [t.get_code(id) for id in ids]
    msg = [('pre', str(i['code']) if i['success'] else str(i['response']['msg'])) for i in raw_codes]
    if account_data.get('notifiers', None):
        if notify_me(title='崩坏：星穹铁道超话签到活动已开启，已为您领取兑换码。', content=msg, notifiers=account_data.get('notifiers', None)):
            return '崩坏：星穹铁道超话签到活动已开启，已为您领取兑换码。请查看私信或私人频道'
    return [str(i['code'] + '\n    ') if i['success'] else str(i['response']['msg'] + '\n    ') for i in raw_codes]

def task7_2(account_data):
    if 'ka_cookie' not in account_data:
        return '请填写 ka_cookie 字段'
    t = sina.Weibo(account_data, setting.zzz_container_id)
    is_event = t.check_event()
    if not is_event:
        return '绝区零超话现在没有活动哦'

    title = '绝区零超话签到提醒'
    content = '亲爱的绳匠, 绝区零微博超话签到活动现已开启, 请注意活动时间! 如已完成任务, 请忽略本信息.'
    notify_me(title, content, account_data.get('notifiers', None))
    ids = t.unclaimed_gift_ids()
    if not ids:
        recent_codes = ' *'.join(
            [f"{i['title']} {i['code']}" for i in t.get_mybox_codes()[:3]])
        msg = '绝区零超话签到活动已开启，但是没有未领取的兑换码。\n    最近 3 个码: '
        if account_data.get('notifiers', None):
            if notify_me(title='', content=msg+f'{recent_codes}', notifiers=account_data.get('notifiers', None)):
                return msg+'请查看私信或私人频道'
        return msg+f'{recent_codes}'

    log.info(f'检测到有 {len(ids)} 个未领取的兑换码')
    raw_codes = [t.get_code(id) for id in ids]
    msg = [('pre', str(i['code']) if i['success'] else str(i['response']['msg'])) for i in raw_codes]
    if account_data.get('notifiers', None):
        if notify_me(title='绝区零超话签到活动已开启，已为您领取兑换码。', content=msg, notifiers=account_data.get('notifiers', None)):
            return '绝区零超话签到活动已开启，已为您领取兑换码。请查看私信或私人频道'
    return [str(i['code'] + '\n    ') if i['success'] else str(i['response']['msg'] + '\n    ') for i in raw_codes]


def task8(account_data):
    if 'cookie' not in account_data:
        return '请填写 cookie 字段'
    is_sign = gh.check_jfsc(account_data['cookie'])
    result = '今天已经签到, 请明天再来'
    if not is_sign:
        r = gh.sign_jfsc(account_data['cookie'])
        result = r.get('msg')
    return result

def task9(account_data):
    import requests
    from genshinhelper.core import get_headers, cookie_to_dict
    import uuid
    if 'cookie' not in account_data:
        return ['请填写 cookie 字段']
    URL_INDEX="https://hk4e-api.mihoyo.com/event/birthdaystar/account/index?badge_uid={badge_uid}&badge_region={badge_region}&game_biz={game_biz}&lang=zh-cn&activity_id=20220301153521"
    URL_ROLES="https://api-takumi.mihoyo.com/binding/api/getUserGameRolesByCookieToken"
    URL_RECEIVE="https://hk4e-api.mihoyo.com/event/birthdaystar/account/post_my_draw?lang=zh-cn&badge_uid={badge_uid}&badge_region={badge_region}&game_biz={game_biz}&activity_id=20220301153521"
    URL_LOGIN="https://api-takumi.mihoyo.com/common/badge/v1/login/account"
    with requests.Session() as session:
        session.headers.update(get_headers())
        if 'useragent' in account_data:
            session.headers['User-Agent'] = account_data['useragent']
        session.cookies.update(cookie_to_dict(account_data['cookie']))
        roles = session.get(URL_ROLES).json()["data"]["list"]
        log.info(f"ROLEs: {roles}")
        result = []
        for role in roles:
            if role['game_biz'] != 'hk4e_cn':
                continue
            uid = role["game_uid"]
            login_result = session.post(URL_LOGIN, json={"game_biz":role['game_biz'],"lang":"zh-cn","region":role["region"],"uid":str(uid)}).json()
            log.info(f"LOGIN: {login_result}")
            get_url = URL_INDEX.format(badge_uid=uid, badge_region=role["region"], game_biz=role['game_biz'])
            url = URL_RECEIVE.format(badge_uid=uid, badge_region=role["region"], game_biz=role['game_biz'])
            g = session.get(get_url).json()
            log.info(f"Index: {g}")
            r = g["data"]['role']
            for i in r:
                if i["is_partake"] == False:
                    payload = {"role_id": int(i['role_id'])}
                    headers = get_headers(with_ds=True, new_ds=True, params=payload, ds_type='android')
                    if 'useragent' in account_data:
                        headers['User-Agent'] = account_data['useragent']
                    headers['x-rpc-device_id'] = str(uuid.uuid3(uuid.NAMESPACE_URL, account_data["cookie"]))
                    headers.update({
                                        "Content-Type": "application/json;charset=UTF-8",
                                        "Referer": "https://webstatic.mihoyo.com/"})
                    t = session.post(url, params=payload, headers=headers)
                    log.info(f"Received: {t.text}")
                    try:
                        log.info(f"📷{g['data']['nick_name']} ({uid}): 领取{i['name']}的画册结果: {t.json().get('message')}）")
                        result.append(f"📷{g['data']['nick_name']} ({uid}): 领取{i['name']}的画册结果: {t.json().get('message')}")
                    except Exception as e:
                        log.info(f"📷{g['data']['nick_name']} ({uid}): 领取{i['name']}的画册失败（{t.text}，{str(e)}）")
                        result.append(f"📷{g['data']['nick_name']} ({uid}): 领取{i['name']}的画册失败（{t.text}，{str(e)}）")
                else:
                    log.info(f"📷{g['data']['nick_name']} ({uid}): 今日{i['name']}的画册已领取")
                    result.append(f"📷{g['data']['nick_name']} ({uid}): 今日{i['name']}的画册已领取")
            if len(r)==0:
                log.info(f"📷{g['data']['nick_name']} ({uid}): 今日没有画册可领取")
                result.append(f"📷{g['data']['nick_name']} ({uid}): 今日没有画册可领取")
        return ["\n    ".join(result)]

task_list = [{
    'name': 'HoYoLAB Community',
    'accounts': config['hoyolab']['accounts'],
    'enabled': config['hoyolab']['bbs']['enable'],
    'function': genshin_checkin
}, {
    'name': '原神签到福利',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['games']['genshin']['checkin']['enable'],
    'function': yuanshen_checkin
}, {
    'name': '崩坏星穹铁道签到福利',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['games']['starrail']['checkin']['enable'],
    'function': starrail_checkin
}, {
    'name': '绝区零签到福利',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['games']['zzz']['checkin']['enable'],
    'function': zzz_checkin
}, {
    'name': '崩坏3福利补给',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['games']['honkai3rd']['checkin']['enable'],
    'function': honkai3rd_checkin
}, {
    'name': '米游币签到姬',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['bbs']['checkin'],
    'function': miyoushe_checkin
}, {
    'name': '云原神签到姬',
    'accounts': config['cloud_games']['genshin']['accounts'],
    'enabled': config['cloud_games']['genshin']['enable'],
    'function': cloud_genshin_checkin
}, {
    'name': '微博超话签到',
    'accounts': config['weibo']['accounts'],
    'enabled': config['weibo']['enable'],
    'function': weibo_checkin
}, {
    'name': '原神超话监测',
    'accounts': config['weibo']['accounts'],
    'enabled': config['weibo']['enable'],
    'function': task7
}, {
    'name': '崩坏：星穹铁道超话监测',
    'accounts': config['weibo']['accounts'],
    'enabled': config['weibo']['enable'],
    'function': task7_1
}, {
    'name': '绝区零超话监测',
    'accounts': config['weibo']['accounts'],
    'enabled': config['weibo']['enable'],
    'function': task7_2
}, {
    'name': '微信积分商城',
    'accounts': config['wechat_shop']['accounts'],
    'enabled': config['wechat_shop']['enable'],
    'function': task8
}, {
    'name': '留影叙佳期',
    'accounts': config['mihoyo']['accounts'],
    'enabled': config['mihoyo']['games']['genshin']['checkin']['enable'] and config['mihoyo']['games']['genshin']['checkin']['birthday'],
    'function': task9
}]


def run_task(name, accounts, enabled, func):
    success_count = 0
    failure_count = 0
    
    if not enabled or not accounts:
        # return a iterable object
        return [success_count, failure_count]

    account_count = len(accounts)
    account_str = 'account' if account_count == 1 else 'accounts'
    log.info(
        'You have {account_count} 「{name}」 {account_str} configured.'.format(
            account_count=account_count, name=name, account_str=account_str))

    result_list = []
    for account_name, account_data in accounts.items():
        log.info('Preparing to perform task for account {}...'.format(account_name))
        raw_result = ''
        try:
            raw_result = func(account_data)
            success_count += 1
        except Exception as e:
            raw_result = e
            log.exception('TRACEBACK')
            failure_count += 1
        finally:
            result_str = "".join(raw_result) if isinstance(raw_result, Iterable) else raw_result
            result_fmt = f'🌈 {account_name}:\n    {result_str}\n'
            result_list.append(result_fmt)
        continue

    task_name_fmt = f'🏆 {name}'
    status_fmt = f'☁️ ✔ {success_count} · ✖ {failure_count}'
    message_box = [success_count, failure_count, task_name_fmt, status_fmt, ''.join(result_list)]
    return message_box


def job1():
    log.info(banner)
    random_sleep(config['general']['random_sleep_secs_range'])
    log.info('Starting...')
    finally_result_dict = {
        i['name']: run_task(i['name'], i['accounts'], i['enabled'], i['function'])
        for i in task_list
    }

    total_success_cnt = sum([i[0] for i in finally_result_dict.values()])
    total_failure_cnt = sum([i[1] for i in finally_result_dict.values()])
    message_list = sum([i[2::] for i in finally_result_dict.values()], [])
    tip = '\nWARNING: Please configure environment variables or config.json file first!\n'
    message_box = '\n'.join(message_list) if message_list else tip

    log.info('RESULT:\n' + message_box)
    if message_box != tip:
        title = f'Genshin Impact Helper ✔ {total_success_cnt} · ✖ {total_failure_cnt}'
        # for notifier, params in config['notifiers'].items():
        #     is_markdown = params.get('markdown', False)
        #     if is_markdown:
        #         content = f'```\n{message_box}```' 
        #     elif notifier in ['telegram']:
        #         content = f'<pre>{message_box}</pre>'
        #     else:
        #         content = message_box
        content = [('pre', message_box)]
        notify_me(title, content)
    log.info('End of process run')


def job2():
    import requests
    from genshinhelper.core import get_headers, cookie_to_dict
    import uuid, json
    result = []
    db = DictPersistJSON(os.path.join(os.path.dirname(__file__), 'config', 'db.json'), cached=True)
    for name, data in config['mihoyo']['accounts'].items():
        cookie = data['cookie']
        ys = gh.YuanShen(cookie)
        if 'useragent' in data:
            ys.headers['User-Agent'] = data['useragent']
        roles_info = retry_until_success(10, lambda : ys.roles_info)
        expedition_fmt = '└─ {character_name:<8} {status_:^8} {remained_time_fmt}\n'
        RESIN_TIMER_TEMPLATE = '''实时便笺 (数据来自米游社API, 可能不准确)
    🔅{nickname} {level} {region_name}
    原粹树脂: {current_resin} / {max_resin} {resin_recovery_datetime_fmt}
    洞天宝钱: {current_home_coin} / {max_home_coin} {home_coin_recovery_datetime_fmt}
    今日委托: {finished_task_num} / {total_task_num}
    周本减半: {remain_resin_discount_num} / {resin_discount_num_limit}
    参量质变仪: {transformer_recovery_fmt}
    探索派遣: {current_expedition_num} / {max_expedition_num}
      {expedition_details}'''

        for i in roles_info:
            daily_note = retry_until_success(10, ys.get_daily_note, i['game_uid'], i['region'])
            for captcha_retry in range(5):
                if 'expeditions' not in daily_note:
                    log.info(f'获取实时便笺失败，疑似验证码，将接入打码平台重试一次')
                    log.info(daily_note)
                    import signin.tools as stools
                    params = {'is_high': 'true'}
                    headers = {
                        "Accept": "application/json, text/plain, */*",
                        "Accept-Encoding": "gzip, deflate",
                        "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
                        "User-Agent": data.get('useragent', setting.headers['User-Agent']),
                        "X-Requested-With": "com.mihoyo.hyperion",
                        "Referer": "https://webstatic.mihoyo.com/",
                        "x-rpc-device_id": stools.get_device_id(data.get('useragent', setting.headers['User-Agent'])),
                        "x-rpc-page": "3.1.3_#/ys",
                    }
                    ds = stools.get_ds2_by_data_params(data=None, params=params)
                    headers["x-rpc-app_version"] = setting.mihoyobbs_version
                    headers["x-rpc-client_type"] = setting.mihoyobbs_Client_type
                    headers["DS"] = ds
                    headers['Cookie'] = cookie
                    # headers.update({"Referer": "https://webstatic.mihoyo.com/"})
                    cap_info = requests.get('https://api-takumi-record.mihoyo.com/game_record/app/card/wapi/createVerification', params=params, headers=headers)
                    # print(cap_info.text)
                    cap_info = cap_info.json()
                    validate = game_record_captcha(cap_info['data']['gt'], cap_info['data']['challenge'])
                    data = {
                        'geetest_challenge': cap_info['data']['challenge'],
                        'geetest_validate': validate,
                        'geetest_seccode': validate+'|jordan'
                    }
                    # print(data)
                    ds = stools.get_ds2_by_data_params(data=data, params=None)
                    headers["x-rpc-app_version"] = setting.mihoyobbs_version
                    headers["x-rpc-client_type"] = setting.mihoyobbs_Client_type
                    headers["DS"] = ds
                    # print(headers)
                    res = requests.post('https://api-takumi-record.mihoyo.com/game_record/app/card/wapi/verifyVerification', json=data, headers=headers)
                    daily_note = retry_until_success(10, ys.get_daily_note, i['game_uid'], i['region'])
                    # print(res.text, daily_note)
                    if not daily_note or 'expeditions' not in daily_note:
                        log.info(f"未能获取 {i['nickname']} 的实时便笺, 正在重试...")
                        continue
                break
            else:
                log.error(f"获取 {i['nickname']} 的实时便笺失败")
                continue

            details = []
            for e in daily_note['expeditions']:
                remained_time = int(e['remained_time'])
                e['remained_time_fmt'] = '{hour}小时{minute}分钟'.format(**minutes_to_hours(remained_time / 60)) if remained_time else ''
                try:
                    e['character_name'] = e['avatar_side_icon'].split('Side_')[1].split('.')[0]
                except:
                    e['character_name'] = 'Unknown'
                e['status_'] = '剩余时间' if e['status'] == 'Ongoing' else '⭐探险完成'
                details.append(expedition_fmt.format(**e))

            daily_note.update(i)
            resin_recovery_time = int(daily_note['resin_recovery_time'])
            resin_recovery_datetime = datetime.datetime.now() + datetime.timedelta(seconds=resin_recovery_time)
            daily_note['resin_recovery_datetime_fmt'] = f"将于{resin_recovery_datetime.strftime('%Y-%m-%d %H:%M:%S')}全部恢复" if daily_note['current_resin']<daily_note['max_resin'] else '⭐原粹树脂已全部恢复, 记得及时使用哦'
            home_coin_recovery_time = int(daily_note['home_coin_recovery_time'])
            home_coin_recovery_datetime = datetime.datetime.now() + datetime.timedelta(seconds=home_coin_recovery_time)
            daily_note['home_coin_recovery_datetime_fmt'] = f"将于{home_coin_recovery_datetime.strftime('%Y-%m-%d %H:%M:%S')}全部恢复" if daily_note['current_home_coin']<daily_note['max_home_coin'] else '⭐洞天宝钱已全部恢复, 记得及时使用哦'
            if daily_note['transformer']['obtained']: 
                if daily_note['transformer']['recovery_time']['reached']:
                    daily_note['transformer_recovery_fmt'] = "⭐参量质变仪已可再次使用, 记得及时使用哦"
                else:
                    daily_note['transformer_recovery_fmt'] = f"距离可再次使用还有{daily_note['transformer']['recovery_time']['Day']}天{daily_note['transformer']['recovery_time']['Hour']}小时{daily_note['transformer']['recovery_time']['Minute']}分钟{daily_note['transformer']['recovery_time']['Second']}秒"
            else:
                daily_note['transformer_recovery_fmt'] = "未获取"
            daily_note['expedition_details'] = '      '.join(details)
            message = RESIN_TIMER_TEMPLATE.format(**daily_note)
            result.append(message)
            log.info(message)

            content = [('pre', message)]
            # if config.ONEPUSH.get('params', {}).get('markdown'):
            #     content = f'```\n{message}```' 
            # elif config.ONEPUSH.get('notifier') in ['telegram']:
            #     content = f'<pre>\n{message}</pre>'
            # else:
            #     content = message
            status = '未满足推送条件, 监控模式运行中...'

            resin_count = config['mihoyo']['games']['genshin']['resource_check']['resin_full_notify_count']
            expedition_count = config['mihoyo']['games']['genshin']['resource_check']['expedition_notify_count']
            coin_count = config['mihoyo']['games']['genshin']['resource_check']['home_coin_full_notify_count']
            transformer_count = config['mihoyo']['games']['genshin']['resource_check']['transformer_ready_notify_count']
            uid = i['game_uid']
            IS_NOTIFY_STR = "IS_NOTIFY_STR"
            RESIN_NOTIFY_CNT_STR = "RESIN_NOTIFY_CNT"
            RESIN_THRESHOLD_NOTIFY_CNT_STR = "RESIN_THRESHOLD_NOTIFY_CNT"
            HOME_COIN_NOTIFY_CNT_STR = "HOME_COIN_NOTIFY_CNT"
            HOME_COIN_THRESHOLD_NOTIFY_CNT_STR = "HOME_COIN_THRESHOLD_NOTIFY_CNT"
            TRANSFORMER_NOTIFY_CNT_STR = "TRANSFORMER_NOTIFY_CNT"
            RESIN_LAST_RECOVERY_TIME = "RESIN_LAST_RECOVERY_TIME"
            EXPEDITION_NOTIFY_CNT_STR = "EXPEDITION_NOTIFY_CNT"
            db[uid] = db.get(uid, dict())
            db[uid][IS_NOTIFY_STR] = False
            db[uid][RESIN_NOTIFY_CNT_STR] = db[uid].get(RESIN_NOTIFY_CNT_STR, 0)
            db[uid][RESIN_THRESHOLD_NOTIFY_CNT_STR] = db[uid].get(RESIN_THRESHOLD_NOTIFY_CNT_STR, 0)
            db[uid][HOME_COIN_NOTIFY_CNT_STR] = db[uid].get(HOME_COIN_NOTIFY_CNT_STR, 0)
            db[uid][HOME_COIN_THRESHOLD_NOTIFY_CNT_STR] = db[uid].get(HOME_COIN_THRESHOLD_NOTIFY_CNT_STR, 0)
            db[uid][TRANSFORMER_NOTIFY_CNT_STR] = db[uid].get(TRANSFORMER_NOTIFY_CNT_STR, 0)

            db[uid][EXPEDITION_NOTIFY_CNT_STR] = db[uid].get(EXPEDITION_NOTIFY_CNT_STR, 0)
            db[uid][RESIN_LAST_RECOVERY_TIME] = db[uid].get(RESIN_LAST_RECOVERY_TIME, resin_recovery_datetime.timestamp())

            is_resin_full = daily_note['current_resin'] >= daily_note['max_resin']
            is_resin_threshold = daily_note['current_resin'] >= config['mihoyo']['games']['genshin']['resource_check']['resin_threshold']
            is_home_coin_full = daily_note['current_home_coin'] >= daily_note['max_home_coin']
            is_home_coin_threshold = daily_note['current_home_coin'] >= config['mihoyo']['games']['genshin']['resource_check']['home_coin_threshold']
            is_transformer_ready = daily_note['transformer']['recovery_time']['reached']
            is_resin_notify = db[uid][RESIN_NOTIFY_CNT_STR] < resin_count
            is_resin_threshold_notify = db[uid][RESIN_THRESHOLD_NOTIFY_CNT_STR] < 1
            is_home_coin_notify = db[uid][HOME_COIN_NOTIFY_CNT_STR] < coin_count
            is_home_coin_threshold_notify = db[uid][HOME_COIN_THRESHOLD_NOTIFY_CNT_STR] < 1
            is_transformer_notify = db[uid][TRANSFORMER_NOTIFY_CNT_STR] < transformer_count
            is_expedition_notify = db[uid][EXPEDITION_NOTIFY_CNT_STR] < expedition_count
            is_do_not_disturb = time_in_range(config['mihoyo']['games']['genshin']['resource_check']['do_not_disturb'])
            is_resin_recovery_time_changed = abs(db[uid][RESIN_LAST_RECOVERY_TIME] - resin_recovery_datetime.timestamp()) > 400

            status = ''
            get_checkmarks = lambda x,y: x*'✅'+max((y-x), 0)*'☑️'
            to_traceback = []
            if is_resin_full and not is_do_not_disturb:
                db[uid][RESIN_NOTIFY_CNT_STR] += 1
                status += '❗原粹树脂回满啦!'+get_checkmarks(db[uid][RESIN_NOTIFY_CNT_STR], resin_count)+'\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_resin_notify
                to_traceback.append(RESIN_NOTIFY_CNT_STR)
            elif is_resin_threshold and not is_do_not_disturb:
                db[uid][RESIN_THRESHOLD_NOTIFY_CNT_STR] += 1
                status += '⚠️原粹树脂快满啦!⏳\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_resin_threshold_notify 
                to_traceback.append(RESIN_THRESHOLD_NOTIFY_CNT_STR)
            elif is_resin_recovery_time_changed and not is_do_not_disturb:
                status += '原粹树脂恢复时间变动啦!⏳\n'
                db[uid][IS_NOTIFY_STR] = True

            if is_home_coin_full and not is_do_not_disturb:
                db[uid][HOME_COIN_NOTIFY_CNT_STR] += 1
                status += '❗洞天宝钱回满啦!'+get_checkmarks(db[uid][HOME_COIN_NOTIFY_CNT_STR], coin_count)+'\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_home_coin_notify 
                to_traceback.append(HOME_COIN_NOTIFY_CNT_STR)
            elif is_home_coin_threshold and not is_do_not_disturb:
                db[uid][HOME_COIN_THRESHOLD_NOTIFY_CNT_STR] += 1
                status += '⚠️洞天宝钱快满啦!⏳\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_home_coin_threshold_notify
                to_traceback.append(HOME_COIN_THRESHOLD_NOTIFY_CNT_STR)

            if daily_note['transformer']['obtained'] and is_transformer_ready and not is_do_not_disturb:
                db[uid][TRANSFORMER_NOTIFY_CNT_STR] += 1
                status += '❗参量质变仪可以用啦!'+get_checkmarks(db[uid][TRANSFORMER_NOTIFY_CNT_STR], transformer_count)+'\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_transformer_notify
                to_traceback.append(TRANSFORMER_NOTIFY_CNT_STR)

            if 'Finished' in str(daily_note['expeditions']) and not is_do_not_disturb:
                db[uid][EXPEDITION_NOTIFY_CNT_STR] += 1
                status += '❗探索派遣完成啦!'+get_checkmarks(db[uid][EXPEDITION_NOTIFY_CNT_STR], expedition_count)+'\n'
                db[uid][IS_NOTIFY_STR] = db[uid][IS_NOTIFY_STR] or is_expedition_notify
                to_traceback.append(EXPEDITION_NOTIFY_CNT_STR)
                                
            
            db[uid][RESIN_NOTIFY_CNT_STR] = db[uid][RESIN_NOTIFY_CNT_STR] if is_resin_full else 0
            db[uid][RESIN_THRESHOLD_NOTIFY_CNT_STR] = db[uid][RESIN_THRESHOLD_NOTIFY_CNT_STR] if is_resin_threshold else 0
            db[uid][HOME_COIN_NOTIFY_CNT_STR] = db[uid][HOME_COIN_NOTIFY_CNT_STR] if is_home_coin_full else 0
            db[uid][HOME_COIN_THRESHOLD_NOTIFY_CNT_STR] = db[uid][HOME_COIN_THRESHOLD_NOTIFY_CNT_STR] if is_home_coin_threshold_notify else 0
            db[uid][TRANSFORMER_NOTIFY_CNT_STR] = db[uid][TRANSFORMER_NOTIFY_CNT_STR] if is_transformer_ready else 0
            db[uid][EXPEDITION_NOTIFY_CNT_STR] = db[uid][EXPEDITION_NOTIFY_CNT_STR] if 'Finished' in str(daily_note['expeditions']) else 0 
            db[uid][RESIN_LAST_RECOVERY_TIME] = resin_recovery_datetime.timestamp()

            title = f'原神签到小助手提醒您: \n{status}'
            log.info(title)
            if db[uid][IS_NOTIFY_STR]:
                notify_me(title, content)
            else:
                for key in to_traceback:
                    db[uid][key] = max(db[uid][key] - 1, 0)
    db.sync()
    log.info('End of process run')
    return result


def run_once():
    for i in dict(os.environ):
        if 'UID_' in i:
            del os.environ[i]

    gh.set_lang(config['general']['language'])
    job1()
    if config['mihoyo']['games']['genshin']['resource_check']['enable']:
        job2()


def main():
    run_once()
    schedule.every().day.at(config['general']['start_time']).do(job1)
    if config['mihoyo']['games']['genshin']['resource_check']['enable']:
        schedule.every(int(config['mihoyo']['games']['genshin']['resource_check']['interval'])).seconds.do(job2)

    while True:
        schedule.run_pending()
        sleep(1)


if __name__ == '__main__':
    main()

