# 米游社的Salt
# mihoyobbs_salt = "egBrFMO1BPBG0UX5XOuuwMRLZKwTVKRV" # iOS?
# mihoyobbs_salt_web = "DG8lqMyc9gquwAUFc7zBS62ijQRX9XF7" # Web?
mihoyobbs_salt_x4 = "xV8v4Qu54lUKrEYFZkJhB8cuOh9Asafs" # Webview
mihoyobbs_salt_x6 = "t0qEgfub6cvueAPgR5m9aQWWVciEer7v" # Android
mihoyobbs_salt = "rtvTthKxEyreVXQCnhluFgLXPOFKPHlA"
mihoyobbs_salt_web = "EJncUPGnOHajenjLhBOsdpwEMZmiCmQX"
# # 米游社的版本
# mihoyobbs_version = "2.49.1"  # Salt和Version相互对应
mihoyobbs_version = "2.71.1"  # Salt和Version相互对应
mihoyobbs_verify_key = 'bll8iq97cem8'
# 米游社的客户端类型
mihoyobbs_Client_type = "2"  # 1为ios 2为安卓
mihoyobbs_Client_type_web = "5"  # 4为pc web 5为mobile web
# 云原神版本
cloudgenshin_Version = "3.0.0"

# 米游社的分区列表
mihoyobbs_List = [{
    "id": "1",
    "forumId": "1",
    "name": "崩坏3",
    "url": "https://bbs.mihoyo.com/bh3/"
}, {
    "id": "2",
    "forumId": "26",
    "name": "原神",
    "url": "https://bbs.mihoyo.com/ys/"
}, {
    "id": "3",
    "forumId": "30",
    "name": "崩坏2",
    "url": "https://bbs.mihoyo.com/bh2/"
}, {
    "id": "4",
    "forumId": "37",
    "name": "未定事件簿",
    "url": "https://bbs.mihoyo.com/wd/"
}, {
    "id": "5",
    "forumId": "34",
    "name": "大别野",
    "url": "https://bbs.mihoyo.com/dby/"
}, {
    "id": "6",
    "forumId": "52",
    "name": "崩坏：星穹铁道",
    "url": "https://bbs.mihoyo.com/sr/"
}, {
    "id": "8",
    "forumId": "57",
    "name": "绝区零",
    "url": "https://bbs.mihoyo.com/zzz/"
}]

game_id2name = {
    "bh2_cn": "崩坏2",
    "bh3_cn": "崩坏3",
    "nxx_cn": "未定事件簿",
    "hk4e_cn": "原神",
    "hkrpg_cn": "崩坏： 星穹铁道",
    "nap_cn": "绝区零"
}
# Config Load之后run里面进行列表的选择
mihoyobbs_List_Use = []

# 游戏签到的请求头
headers = {
    'Accept': 'application/json, text/plain, */*',
    'DS': "",
    "x-rpc-channel": "miyousheluodi",
    'Origin': 'https://webstatic.mihoyo.com',
    'x-rpc-app_version': mihoyobbs_version,
    'User-Agent': 'Mozilla/5.0 (Linux; Android 12; Unspecified Device) AppleWebKit/537.36 (KHTML, like Gecko) '
                  f'Version/4.0 Chrome/103.0.5060.129 Mobile Safari/537.36 miHoYoBBS/{mihoyobbs_version}',
    'x-rpc-client_type': mihoyobbs_Client_type_web,
    'Referer': '',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,en-US;q=0.8',
    'X-Requested-With': 'com.mihoyo.hyperion',
    "Cookie": "",
    'x-rpc-device_id': ""
}

# 通用设置
bbs_Api = "https://bbs-api.mihoyo.com"
web_Api = "https://api-takumi.mihoyo.com"
account_Info_url = web_Api + "/binding/api/getUserGameRolesByCookie?game_biz="

# 米游社的API列表
bbs_Cookie_url = "https://webapi.account.mihoyo.com/Api/cookie_accountinfo_by_loginticket?login_ticket={}"
bbs_Cookie_url2 = web_Api + "/auth/api/getMultiTokenByLoginTicket?login_ticket={}&token_types=3&uid={}"
bbs_get_cookie_token_by_stoken = "https://api-takumi.mihoyo.com/auth/api/getCookieAccountInfoBySToken"
bbs_Tasks_list = bbs_Api + "/apihub/sapi/getUserMissionsState"  # 获取任务列表
bbs_Sign_url = bbs_Api + "/apihub/app/api/signIn"  # post
bbs_List_url = bbs_Api + "/post/api/getForumPostList?forum_id={}&is_good=false&is_hot=false&page_size=20&sort_type=1"
bbs_Detail_url = bbs_Api + "/post/api/getPostFull?post_id={}"
bbs_Share_url = bbs_Api + "/apihub/api/getShareConf?entity_id={}&entity_type=1"
bbs_Like_url = bbs_Api + "/apihub/sapi/upvotePost"  # post json
bbs_get_captcha = bbs_Api + "/misc/api/createVerification?is_high=true"
bbs_captcha_verify = bbs_Api + "/misc/api/verifyVerification"

# 通用游戏签到API
any_checkin_rewards = web_Api + '/event/luna/home?lang=zh-cn&act_id={}'
any_is_signurl = web_Api + "/event/luna/info?lang=zh-cn&act_id={}&region={}&uid={}"
any_sign_url = web_Api + "/event/luna/sign"
# 崩坏2自动签到相关的相关设置
honkai2_Act_id = "e202203291431091"
honkai2_checkin_rewards = any_checkin_rewards.format(honkai2_Act_id)
honkai2_Is_signurl = any_is_signurl
honkai2_Sign_url = any_sign_url

# 崩坏3自动签到相关的设置
honkai3rd_Act_id = "e202207181446311"
honkai3rd_checkin_rewards = any_checkin_rewards.format(honkai3rd_Act_id)
honkai3rd_Is_signurl = any_is_signurl
honkai3rd_Sign_url = any_sign_url

# 未定事件簿自动签到相关设置
tearsofthemis_Act_id = "e202202251749321"
tearsofthemis_checkin_rewards = any_checkin_rewards.format(tearsofthemis_Act_id)
tearsofthemis_Is_signurl = any_is_signurl
tearsofthemis_Sign_url = any_sign_url  # 和二崩完全一致

# 原神自动签到相关的设置
# genshin_Act_id = "e202009291139501"
genshin_Act_id = "e202311201442471"
# genshin_checkin_rewards = f'{web_Api}/event/bbs_sign_reward/home?act_id={genshin_Act_id}'
# genshin_Is_signurl = web_Api + "/event/bbs_sign_reward/info?act_id={}&region={}&uid={}"
# genshin_Signurl = web_Api + "/event/bbs_sign_reward/sign"
genshin_checkin_rewards = any_checkin_rewards.format(genshin_Act_id)
genshin_Is_signurl = any_is_signurl
genshin_Signurl = any_sign_url
genshin_diary_url = "https://hk4e-api.mihoyo.com/event/ys_ledger/monthInfo?bind_uid={}&bind_region={}&month=0&bbs_presentation_style=fullscreen&bbs_auth_required=true&mys_source=GameRecord"

# 崩坏：星穹铁道自动签到相关的设置
honkai_sr_Act_id = "e202304121516551"
honkai_sr_checkin_rewards = any_checkin_rewards.format(honkai_sr_Act_id)
honkai_sr_Is_signurl = any_is_signurl
honkai_sr_Sign_url = any_sign_url
honkai_sr_diary_url = web_Api + "/event/srledger/month_info?uid={}&region={}&month="

# 绝区零自动签到相关的设置
zzz_web_api = 'https://act-nap-api.mihoyo.com'
zzz_Act_id = "e202406242138391"
zzz_checkin_rewards = f"{zzz_web_api}/event/luna/zzz/home?lang=zh-cn&act_id={{}}".format(zzz_Act_id)
zzz_Is_signurl = f"{zzz_web_api}/event/luna/zzz/info?lang=zh-cn&act_id={{}}&region={{}}&uid={{}}"
zzz_Sign_url = f"{zzz_web_api}/event/luna/zzz/sign"
zzz_diary_url = None

# 云原神相关api
cloud_genshin_Api = "https://api-cloudgame.mihoyo.com"
cloud_genshin_sgin = cloud_genshin_Api + "/hk4e_cg_cn/wallet/wallet/get"


# 微博相关
genshin_container_id = '100808fc439dedbb06ca5fd858848e521b8716' #原神超话ID
honkai_sr_container_id = '100808e1f868bf9980f09ab6908787d7eaf0f0' #崩铁超话ID
zzz_container_id = '100808f303ad099b7730ad1f96ff49726d3ff3' #绝零超话ID