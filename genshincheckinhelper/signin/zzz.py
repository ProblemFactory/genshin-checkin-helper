
import setting
import config
from signin.game import GameCheckin


class ZZZ(GameCheckin):
    '''
    绝区零签到
    '''
    def __init__(self, account_data) -> None:
        super(ZZZ, self).__init__('nap_cn', setting.zzz_checkin_rewards, account_data, config.config['mihoyo']["games"]["zzz"]['checkin'])
        self.act_id = setting.zzz_Act_id
        self.sign_api = setting.zzz_Sign_url
        self.is_sign_api = setting.zzz_Is_signurl
        self.diary_api = setting.zzz_diary_url
        self.rewards_api = setting.zzz_checkin_rewards
        self.headers['Referer'] = 'https://act.mihoyo.com/'
        self.headers["Origin"] = "https://act.mihoyo.com"
        self.headers["x-rpc-signgame"] = "zzz"
        self.game_mid = "zzz"
        self.game_name = "绝区零"
        self.player_name = "绳匠"
        