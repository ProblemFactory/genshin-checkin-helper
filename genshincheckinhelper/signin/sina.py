"""
@Project   : genshinhelper
@Author    : y1ndan
@Blog      : https://www.yindan.me
@GitHub    : https://github.com/y1ndan
"""

import re
from urllib.parse import  unquote

from bs4 import BeautifulSoup

from genshinhelper.utils import request, nested_lookup, cookie_to_dict
import requests
import signin.captcha as captcha


class Weibo(object):
    def __init__(self, account_data, container_id):
        """
        weibo_cookie: s=xxxxxx; gsid=xxxxxx; aid=xxxxxx; from=xxxxxx
        ka_cookie: aid, from, SUB, SUBP (https://games.weibo.cn/prize/lottery?ticket_id=1641&ext=)
        """
        self.params = cookie_to_dict(account_data['weibo_cookie'].replace('&', ';')) if account_data.get('weibo_cookie','') else None
        self.cookie = cookie_to_dict(account_data.get('ka_cookie', None))
        self.cookie.update(self.params)
        user_headers = account_data.get('headers',dict())
        self.ua = user_headers.get('User-Agent', 'WeiboOverseas/4.4.6 (iPhone; iOS 14.0.1; Scale/2.00)')
        self.headers = {'User-Agent': self.ua}
        self.headers.update(user_headers)
        self.container_id = container_id
        self.browser_headers = account_data.get('browser_headers', None) or self.headers
        self.follow_data_url = 'https://api.weibo.cn/2/cardlist'
        self.sign_url = 'https://api.weibo.cn/2/page/button'
        self.event_url = f'https://m.weibo.cn/api/container/getIndex?containerid={self.container_id}_-_activity_list'
        self.mybox_url = 'https://ka.sina.com.cn/html5/mybox'
        self.draw_url = 'https://games.weibo.cn/prize/aj/lottery'
        self._follow_data = []
        self.signin_data = self.signin()

    def signin(self):
        signin_data = request('POST', 'https://api.weibo.cn/2/account/login', data=self.cookie|{'getcookie':'1'}).json()
        return signin_data
    
    def extract_cookie(self, url: str):
        domain = re.search(r'(?<=//)[^/]+', url).group()
        result = dict()
        for key, cks in self.signin_data.get('cookie', dict()).get('cookie', dict()).items():
            if domain.endswith(key):
                for ck in cks.split('\n'):
                    ck = ck.split(';')[0]
                    result[ck[:ck.index('=')]] = ck[ck.index('=')+1:]
        return result

    @property
    def follow_data(self):
        if not self._follow_data:
            url = self.follow_data_url
            self.params['containerid'] = '100803_-_followsuper'
            # turn off certificate verification
            response = request('get', url, params=self.params, headers=self.headers, cookies=self.cookie).json()
            card_group = nested_lookup(response, 'card_group', fetch_first=True)
            follow_list = [i for i in card_group if i['card_type'] == '8']
            for i in follow_list:
                action = nested_lookup(i, 'action', fetch_first=True)
                request_url = ''.join(
                    re.findall('request_url=(.*)%26container', action)) if action else None
                follow = {
                    'name': nested_lookup(i, 'title_sub', fetch_first=True),
                    'level': int(re.findall(r'\d+', i['desc1'])[0]),
                    'is_sign': False if nested_lookup(i, 'name', fetch_first=True) == '签到' else True,
                    'request_url': request_url
                }
                self._follow_data.append(follow)

            self._follow_data.sort(key=lambda k: (k['level']), reverse=True)
        return self._follow_data

    def sign(self):
        result = []
        for follow in self.follow_data:
            if not follow['is_sign']:
                url = self.sign_url
                self.params['request_url'] = follow['request_url']
                if self.params.get('containerid'):
                    del self.params['containerid']
                # turn off certificate verification
                for _ in range(10):
                    try:
                        response = request('get', url, params=self.params, headers=self.headers)
                        response_json = response.json()
                        follow['sign_response'] = response_json
                        if int(response_json.get('result', -1)) == 1:
                            follow['is_sign'] = True
                            follow['request_url'] = None
                            break
                        elif int(response_json.get('result', -1)) == 0:
                            follow['sign_response']['captcha']=True
                            browser_ua = self.browser_headers.get('User-Agent')
                            geetest_info = request('get', response_json['scheme'], headers=self.browser_headers, params={'ua':browser_ua}, cookies=self.extract_cookie(response_json['scheme']))
                            # next_url = re.findall('\"(https://security.weibo.com/captcha/geetest.*)\"', tmp.text)[0]
                            # tmp = request('get', next_url, headers=self.browser_headers|{'Referer':next_url})
                            if '/captcha/ajgeetest' in geetest_info.text:
                                cap_init_url = 'https://security.weibo.com'+re.findall('[\'\"](/captcha/ajgeetest\?action=init.*)[\'\"]', geetest_info.text)[0]
                                cap_val_url = 'https://security.weibo.com'+re.findall('[\'\"](/captcha/ajgeetest\?action=validate.*)[\'\"]', geetest_info.text)[0]
                            else:
                                cap_key = response_json['scheme'][response_json['scheme'].index('key=')+4:]+'&'
                                cap_key = cap_key[:cap_key.index('&')]
                                cap_init_url = 'https://security.weibo.com/captcha/ajgeetest?action=init&key='+cap_key
                                cap_val_url = 'https://security.weibo.com/captcha/ajgeetest?action=validate&key='+cap_key
                            cap_page = request('get', cap_init_url, headers=self.browser_headers|{'Referer':geetest_info.url}, cookies=self.extract_cookie(cap_init_url))
                            cap_info = cap_page.json()
                            if 'data' not in cap_info or 'gt' not in cap_info['data'] or 'challenge' not in cap_info['data']:
                                follow['is_sign'] = False
                                follow['sign_response'] = 'Failed to sign due to unknown captcha status: '+str(follow.get('sign_response', '')+'\t\t\t'+str(cap_info))
                                break
                            cap_info = cap_info['data']
                            validate = captcha.general_captcha(cap_info['gt'], cap_info['challenge'], 'https://security.weibo.com')
                            cap_data = {
                                "geetest_challenge":cap_info['challenge'],
                                "geetest_validate":validate,
                                "geetest_seccode":validate+'|jordan'}
                            tmp = request('post', cap_val_url, headers=self.browser_headers|{'Referer':geetest_info.url}, data=cap_data, cookies=self.extract_cookie(cap_val_url))
                            if tmp.text.startswith('{') and tmp.json()['retcode']==100000:
                                follow['sign_response']['captcha']=False
                            else:
                                print('Weibo captcha failed: '+str(tmp.content))
                                follow['sign_response'] = 'Failed to sign due to unable to solve captcha: '+str(follow.get('sign_response', ''))
                        else:
                            follow['is_sign'] = False
                            follow['sign_response'] = 'Failed to sign: '+str(follow.get('sign_response', ''))
                            break
                    except Exception as e:
                        follow['sign_response'] = 'Failed to sign due to exception: '+str(e)+", "+str(follow.get('sign_response', ''))
            result.append(follow)
        return result

    @property
    def event_list(self):
        url = self.event_url
        response = request('get', url).json()
        return nested_lookup(response, 'group', fetch_first=True)

    def check_event(self):
        return True if "签到" in repr(self.event_list) else False

    def get_event_gift_ids(self):
        return [
            i
            for event in self.event_list
            for i in re.findall(r'ticket_id=(\d*)', unquote(unquote(event['scheme'])))
        ]

    def get_mybox_codes(self):
        url = self.mybox_url
        response = request('get', url, headers=self.headers, cookies=self.cookie, allow_redirects=False)
        if response.status_code != 200:
            raise Exception(
                'Failed to get my box codes: '
                'The cookie seems to be invalid, please re-login to https://ka.sina.com.cn'
            )

        response.encoding = 'utf-8'
        soup = BeautifulSoup(response.text, 'html.parser')
        # print(soup.prettify())
        boxs = soup.find_all(class_='giftbag')
        mybox_codes = []
        for box in boxs:
            item = {
                'id': box.find(class_='deleBtn').get('data-itemid'),
                'title': box.find(class_='title itemTitle').text,
                'code': '`'+box.find('span').parent.contents[1]+'`'
            }
            mybox_codes.append(item)
        return mybox_codes

    def unclaimed_gift_ids(self):
        event_gift_ids = self.get_event_gift_ids()
        #mybox_gift_ids = [item.get('id') for item in self.get_mybox_codes()]
        #return [i for i in event_gift_ids if i not in mybox_gift_ids]
        return event_gift_ids

    def get_code(self, id: str):
        url = self.draw_url
        self.headers.update({
            'Referer': f'https://ka.sina.com.cn/html5/gift/{id}'
        })
        data = {
            'ext': '', 'ticket_id': id, 'aid': self.cookie['aid'], 'from': self.cookie['from']
        }
        response = request('get', url, params=data, headers=self.headers, cookies=self.cookie)
        response = response.json()
        code = '`'+response['data']['prize_data']['card_no']+'`' if response['msg'] == 'success' or response['msg'] == 'recently' else False
        if response['msg'] == 'fail':
            response['msg'] = response['data']['fail_desc1']
        result = {'success': True, 'id': id, 'code': code} if code else {'success': False, 'id': id, 'response': response}
        return result