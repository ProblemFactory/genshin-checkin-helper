
import setting
import config
from signin.game import GameCheckin


class StarRail(GameCheckin):
    '''
    崩坏星穹铁道签到
    '''
    def __init__(self, account_data) -> None:
        super(StarRail, self).__init__("hkrpg_cn", setting.any_checkin_rewards.format(setting.honkai_sr_Act_id), account_data, config.config['mihoyo']["games"]["genshin"]['checkin'])
        self.act_id = setting.honkai_sr_Act_id
        self.diary_api = setting.honkai_sr_diary_url
        self.headers['Referer'] = f'https://webstatic.mihoyo.com/bbs/event/signin/hkrpg/index.html?' \
                                  f'act_id={self.act_id}&bbs_auth_required=true&bbs_presentation_style' \
                                  f'=fullscreen&utm_source=share&utm_medium=bbs&utm_campaign=app'
        # self.headers['Referer'] = 'https://act.mihoyo.com/'
        # self.headers["Origin"] = "https://act.mihoyo.com"
        self.headers["x-rpc-signgame"] = "hkrpg_cn"
        self.game_mid = "honkai_sr"
        self.game_name = "崩坏: 星穹铁道"
        self.player_name = "开拓者"
        