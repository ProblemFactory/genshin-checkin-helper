import time
import signin.tools as tools
import random
import signin.captcha as captcha
from account import update_cookie_token
from error import *
from request import http
from loghelper import log
from account import get_account_list
import setting
import datetime


class GameCheckin:
    '''
    米哈游游戏米游社签到
    '''
    def __init__(self, game_id, rewards_api, account_data, config) -> None:
        self.game_id = game_id
        self.rewards_api = rewards_api
        self.config = config
        self.account_data = account_data
        self.headers = setting.headers
        self.headers['DS'] = tools.get_ds(web=True)
        
        self.headers['Cookie'] = account_data['cookie']
        self.headers['x-rpc-device_id'] = tools.get_device_id(account_data['cookie'])
        self.headers['User-Agent'] = tools.get_useragent(account_data.get('useragent',''))
        self.account_list = get_account_list(self.game_id, self.headers, raw=True)
        if len(self.account_list) != 0:
            self.checkin_rewards = self.get_checkin_rewards()
        self.is_sign_api = setting.any_is_signurl
        self.sign_api = setting.any_sign_url
        self.diary_api = None
        self.act_id = ""
        self.game_mid = ""
        self.game_name = ""
        self.player_name = "玩家"

    # 获取已经签到奖励列表
    def get_checkin_rewards(self) -> list:
        for i in range(5):
            log.info("正在获取签到奖励列表...")
            req = http.get(self.rewards_api, headers=self.headers)
            data = req.json()
            if data["retcode"] != 0:
                log.warning("出现{}错误，重试中。。。{}/{}".format(data['retcode'], i, 5))
                time.sleep(5)
                continue
            return data["data"]["awards"]
        log.warning("获取签到奖励列表失败")
        print(req.text)
        return [{"name":'❔', 'cnt':'❔'}]*31
        

    # 判断签到
    def is_sign(self, region: str, uid: str, update = False) -> dict:
        for i in range(5):
            req = http.get(self.is_sign_api.format(self.act_id, region, uid), headers=self.headers)
            data = req.json()
            if data["retcode"] != 0:
                if not update and update_cookie_token(self.account_data):
                    self.headers['Cookie'] = self.account_data['cookie']
                log.warning("出现{}错误，重试中。。。{}/{}".format(data['retcode'], i, 5))
                time.sleep(5)
                continue
            return data["data"]
        raise CookieError("is_sign Error: "+str(data))

    def check_in(self, account):
        header = {}
        header.update(self.headers)
        for i in range(11):
            if i != 0:
                log.info(f'触发验证码，即将进行第{i}次重试，最多10次')
            req = http.post(url=self.sign_api, headers=header,
                            json={'act_id': self.act_id, 'region': account['region'], 'uid': account['game_uid']})
            if req.status_code == 429:
                time.sleep(10)  # 429同ip请求次数过多，尝试sleep10s进行解决
                log.warning(f'429 Too Many Requests ，即将进入下一次请求')
                continue
            data = req.json()
            if data["retcode"] == 0 and data["data"]["success"] == 1:
                validate = captcha.game_captcha(data["data"]["gt"], data["data"]["challenge"])
                if validate is not None:
                    header["x-rpc-challenge"] = data["data"]["challenge"]
                    header["x-rpc-validate"] = validate
                    header["x-rpc-seccode"] = f'{validate}|jordan'
                time.sleep(random.randint(6, 15))
            else:
                break
        return req

    # 签到
    def sign_account(self) -> list:
        return_data = []
        if len(self.account_list) != 0:
            for account in self.account_list:
                return_data.append(
                    {
                        'today': datetime.date.today(), #is_sign
                        'nickname': '', #account
                        'level': '', #account
                        'region_name': '', #account
                        'reward_name': '',
                        'reward_cnt': '',
                        'total_sign_day': '', #is_sign
                        'status': '',
                        'addons': '',
                        'end': ''
                    }
                )
                return_data[-1].update(account)
                if account['game_uid'] in self.config['black_list']:
                    continue
                log.info(f"正在为{account['nickname']}进行签到...")
                time.sleep(random.randint(2, 8))
                is_data = self.is_sign(region=account['region'], uid=account['game_uid'])
                if is_data.get("first_bind", False):
                    log.warning(f"{account['nickname']}是第一次绑定米游社，请先手动签到一次")
                else:
                    return_data[-1].update(is_data)
                    sign_days = is_data["total_sign_day"] - 1
                    ok = True
                    return_data[-1]['reward_name'] = self.checkin_rewards[sign_days]['name']
                    return_data[-1]['reward_cnt'] = self.checkin_rewards[sign_days]['cnt']
                    if is_data["is_sign"]:
                        log.info(f"{account['nickname']}今天已经签到过了~\r\n今天获得的奖励是{tools.get_item(self.checkin_rewards[sign_days])}")
                        sign_days += 1
                    else:
                        time.sleep(random.randint(2, 8))
                        req = self.check_in(account)
                        if req.status_code != 429:
                            data = req.json()
                            if data["retcode"] == 0 and data["data"]["success"] == 0:
                                log.info(f"{account['nickname']}签到成功~\r\n今天获得的奖励是"
                                         f"{tools.get_item(self.checkin_rewards[0 if sign_days == 0 else sign_days + 1])}")
                                return_data[-1]['reward_name'] = self.checkin_rewards[0 if sign_days == 0 else sign_days + 1]['name']
                                return_data[-1]['reward_cnt'] = self.checkin_rewards[0 if sign_days == 0 else sign_days + 1]['cnt']
                                sign_days += 2
                            elif data["retcode"] == -5003:
                                log.info(
                                    f"{account['nickname']}今天已经签到过了~\r\n今天获得的奖励是{tools.get_item(self.checkin_rewards[sign_days])}")
                            else:
                                s = "账号签到失败！"
                                if data["data"] != "" and data.get("data").get("success", -1):
                                    s += "原因: 验证码\njson信息:" + req.text
                                log.warning(s)
                                ok = False
                        else:
                            ok = False
                            data = {}
                    if ok:
                        return_data[-1]['status'] = 'OK'
                    else:
                        if data.get("data") is not None and data.get("data").get("success", -1) == 1:
                            return_data[-1]['status'] = '❌Captcha!'
                        else:
                            return_data[-1]['status'] = '❌Unexpected Error!'
                    return_data[-1]['total_sign_day'] = sign_days
        else:
            return_data = [{'status':'❌No Account', 'total_sign_day':0}]
        return return_data
    
    def get_diary_info(self, required_keys: set = None):
        result = []
        default_result = {k:'' for k in required_keys} if required_keys is not None else {}
        for account in self.account_list:
            region = account['region']
            uid = account['game_uid']
            if self.diary_api is None:
                result.append(default_result)
                continue
            req = http.get(self.diary_api.format(uid, region), headers=self.headers)
            data = req.json()
            if data["retcode"] != 0:
                log.warning("获取账号月历信息失败！")
                print(req.text)
                result.append(default_result)
            else:
                if required_keys is None:
                    result.append(data["data"])
                else:
                    result.append(tools.extract_subset_of_dict(tools.extract_subset_of_dict(data["data"], {'month_data'})['month_data'], required_keys))
        return result