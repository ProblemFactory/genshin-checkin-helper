
import setting
import config
from signin.game import GameCheckin


class YuanShen(GameCheckin):
    '''
    原神签到
    '''
    def __init__(self, account_data) -> None:
        super(YuanShen, self).__init__('hk4e_cn', setting.genshin_checkin_rewards, account_data, config.config['mihoyo']["games"]["genshin"]['checkin'])
        self.act_id = setting.genshin_Act_id
        self.sign_api = setting.genshin_Signurl
        self.is_sign_api = setting.genshin_Is_signurl
        self.diary_api = setting.genshin_diary_url
        self.headers['Referer'] = 'https://act.mihoyo.com/'
        self.headers["Origin"] = "https://act.mihoyo.com"
        self.headers["x-rpc-signgame"] = "hk4e"
        self.game_mid = "genshin"
        self.game_name = "原神"
        self.player_name = "旅行者"
        