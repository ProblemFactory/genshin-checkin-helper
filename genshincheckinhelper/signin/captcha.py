from request import http
import config


def game_record_captcha(gt: str, challenge: str):
    return rrocr(gt, challenge, 'https://webstatic.mihoyo.com/', config.config['general']['rrocr_token'])
    try:
        validate = geetest(gt, challenge, 'https://webstatic.mihoyo.com/', config.config['general']['coolxi_token'])
        assert validate is not None, ''
        return validate  # 失败返回None 成功返回validate
    except Exception as e:
        print('酷曦验证码识别失败（{}），尝试使用rrocr: '.format(str(e)))
        return rrocr(gt, challenge, 'https://webstatic.mihoyo.com/', config.config['general']['rrocr_token'])


def game_captcha(gt: str, challenge: str):
    return rrocr(gt, challenge, 'https://passport-api.mihoyo.com/account/ma-cn-passport/app/loginByPassword', config.config['general']['rrocr_token'])
    try:
        validate = geetest(gt, challenge, 'https://passport-api.mihoyo.com/account/ma-cn-passport/app/loginByPassword', config.config['general']['coolxi_token'])
        assert validate is not None, ''
        return validate  # 失败返回None 成功返回validate
    except Exception as e:
        print('酷曦验证码识别失败（{}），尝试使用rrocr: '.format(str(e)))
        return rrocr(gt, challenge, 'https://passport-api.mihoyo.com/account/ma-cn-passport/app/loginByPassword', config.config['general']['rrocr_token'])


def bbs_captcha(gt: str, challenge: str):
    return rrocr(gt, challenge,
                    "https://webstatic.mihoyo.com/bbs/event/signin-ys/index.html?bbs_auth_required=true&act_id"
                    "=e202009291139501&utm_source=bbs&utm_medium=mys&utm_campaign=icon",
                    config.config['general']['rrocr_token'])
    try:
        validate = geetest(gt, challenge,
                        "https://webstatic.mihoyo.com/bbs/event/signin-ys/index.html?bbs_auth_required=true&act_id"
                        "=e202009291139501&utm_source=bbs&utm_medium=mys&utm_campaign=icon",
                        config.config['general']['coolxi_token'])
        assert validate is not None, ''
        return validate  # 失败返回None 成功返回validate
    except Exception as e:
        print('酷曦验证码识别失败（{}），尝试使用rrocr: '.format(str(e)))
        return rrocr(gt, challenge,
                       "https://webstatic.mihoyo.com/bbs/event/signin-ys/index.html?bbs_auth_required=true&act_id"
                       "=e202009291139501&utm_source=bbs&utm_medium=mys&utm_campaign=icon",
                       config.config['general']['rrocr_token'])


def general_captcha(gt: str, challenge: str, referer: str):
    return rrocr(gt, challenge, referer, config.config['general']['rrocr_token'])
    try:
        validate = geetest(gt, challenge, referer, config.config['general']['coolxi_token'])
        assert validate is not None, ''
        return validate  # 失败返回None 成功返回validate
    except Exception as e:
        print('酷曦验证码识别失败（{}），尝试使用rrocr: '.format(str(e)))
        return rrocr(gt, challenge, referer, config.config['general']['rrocr_token'])

def geetest(gt: str, challenge: str, referer: str, token: str):
    response = http.post('https://api.ocr.kuxi.tech/api/recognize', params={
        'gt': gt,
        'challenge': challenge,
        'referer': referer,
        'token': token
    })
    data = response.json()
    if data['code'] != 0:
        print(data['msg'])
        return None
    return data['data']['validate']  # 失败返回None 成功返回validate

def rrocr(gt: str, challenge: str, referer: str, token: str):
    MAX_RETRIES = 5
    for i in range(MAX_RETRIES):
        try:
            if not token:
                return None
            # print({
            #     'gt': gt,
            #     'challenge': challenge,
            #     'referer': referer,
            #     'appkey': token
            # })
            proxies = None
            if config.config["general"]["rrocr_proxy"]:
                proxies = {
                    'http': config.config["general"]["rrocr_proxy"],
                    'https': config.config["general"]["rrocr_proxy"]
                }
            response = http.post('http://api.rrocr.com/api/recognize.html', params={
                'gt': gt,
                'challenge': challenge,
                'referer': referer,
                'appkey': token
            }, proxies=proxies)
            # print(response, response.text)
            data = response.json()
            if data['status'] != 0:
                print('rrocr识别失败（{}）, 重试 {}/{}'.format(data['msg'], i, MAX_RETRIES))
                continue
            return data['data']['validate']
        except Exception as e:
            print('rrocr识别失败（异常：{}）, 重试 {}/{}'.format(str(e), i, MAX_RETRIES))
            pass
    return None