from genshinhelper.utils import log
from random import randint
from threading import Timer
from time import sleep
import os
import json
import datetime
import uuid
import hashlib
import random
import time
import string
from typing import Mapping, Any, Optional


# class HeaderHelper:
#     '''
#     生成请求头
#     '''
#     def __init__(self, ua, version='2.41.2'):
#         self.ua = ua or self.__class__.get_ua(version)
#         if 'miHoYoBBS/' not in self.ua:
#             self.ua = self.ua + ' miHoYoBBS/'+version
#         self.headers = {
#             "Accept": "application/json, text/plain, */*",
#             "Accept-Encoding": "gzip, deflate",
#             "Accept-Language": "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7",
#             "User-Agent": self.ua,
#             "X-Requested-With": "com.mihoyo.hyperion",
#             "Referer": "https://webstatic.mihoyo.com/",
#             "x-rpc-device_id": self.get_device_id(self.ua),
#             "x-rpc-page": "3.1.3_#/ys",
#         }
        
#     @classmethod
#     def get_device_id(cls, name: str = ""):
#         return str(uuid.uuid3(uuid.NAMESPACE_URL, name))

#     @classmethod
#     def get_ua(cls, device: str = "Pixel Build", version: str = "2.36.1"):
#         return (
#             f"Mozilla/5.0 (Linux; Android 12; {device}; wv) "
#             "AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/103.0.5060.129 Mobile Safari/537.36 "
#             f"{'miHoYoBBS/' + version if version else ''}"
#         )

#     @classmethod
#     def hex_digest(cls, text):
#         _md5 = hashlib.md5()  # nosec B303
#         _md5.update(text.encode())
#         return _md5.hexdigest()

#     @classmethod
#     def get_ds(cls, ds_type: str = None, new_ds: bool = False, data: Any = None, params: Optional[Mapping[str, Any]] = None):
#         """DS 算法
#         代码来自 https://github.com/y1ndan/genshinhelper
#         :param ds_type:  1:ios  2:android  4:pc web  5:mobile web
#         :param new_ds: 是否为DS2算法
#         :param data: 需要签名的Data
#         :param params: 需要签名的Params
#         :return:
#         """

#         def new():
#             t = str(int(time.time()))
#             r = str(random.randint(100001, 200000))  # nosec
#             b = json.dumps(data) if data else ""
#             q = "&".join(f"{k}={v}" for k, v in sorted(params.items())) if params else ""
#             c = cls.hex_digest(f"salt={salt}&t={t}&r={r}&b={b}&q={q}")
#             return f"{t},{r},{c}"

#         def old():
#             t = str(int(time.time()))
#             r = "".join(random.sample(string.ascii_lowercase + string.digits, 6))
#             c = cls.hex_digest(f"salt={salt}&t={t}&r={r}")
#             return f"{t},{r},{c}"

#         '''
#             IOS = 1,
#             ANDROID = 2,
#             WEB = 4,     // pc
#             WEBVIEW = 5, // webview in android / ios app
#         '''

#         app_version = "2.41.2"
#         client_type = "5"
#         salt = "osgT0DljLarYxgebPPHJFjdaxPfoiHGt" # likely WRONG
#         ds = old()
#         if ds_type in ("android", "2"):
#             app_version = "2.41.2"
#             client_type = "2"
#             salt = "TsmyHpZg8gFAVKTtlPaL6YwMldzxZJxQ" # likely WRONG
#             ds = old()
#         if ds_type == "android_new":
#             app_version = "2.41.2"
#             client_type = "2"
#             salt = "xV8v4Qu54lUKrEYFZkJhB8cuOh9Asafs" # likely WRONG
#             ds = new()
#         if new_ds:
#             app_version = "2.41.2"
#             client_type = "5"
#             salt = "t0qEgfub6cvueAPgR5m9aQWWVciEer7v" # likely WRONG
#             ds = new()

#         return app_version, client_type, ds

class DictPersistJSON(dict):
    '''
    json but sync to disk
    use 'cached' to control when to sync to disk
    '''
    def __init__(self, filename, cached, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        self.__cahced = cached
        self._load()
        if not self.__cahced:
            self._dump()

    def _load(self):
        if os.path.isfile(self.filename) and os.path.getsize(self.filename) > 0:
            with open(self.filename, 'r', encoding='UTF-8') as fh:
                self.update(json.load(fh))

    def _dump(self):
        with open(self.filename, 'w', encoding='UTF-8') as fh:
            json.dump(self, fh)

    def __getitem__(self, key):
        return dict.__getitem__(self, key)

    def __setitem__(self, key, val):
        dict.__setitem__(self, key, val)
        if not self.__cahced:
            self._dump()

    def __repr__(self):
        dictrepr = dict.__repr__(self)
        return '%s(%s)' % (type(self).__name__, dictrepr)

    def update(self, *args, **kwargs):
        for k, v in dict(*args, **kwargs).items():
            self[k] = v
        if not self.__cahced:
            self._dump()
    
    def sync(self):
        self._dump()


class Watchdog(Exception):
    '''
    Watchdog class for handling timeout
    '''
    def __init__(self, timeout, userHandler=None):  # timeout in seconds
        super().__init__()
        self.timeout = timeout
        self.handler = userHandler if userHandler is not None else self.defaultHandler
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def reset(self):
        self.timer.cancel()
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def stop(self):
        self.timer.cancel()

    def defaultHandler(self):
        raise self

def retry_until_success(timeout, f, *args, **kwargs):
    '''
    retry until success
    '''
    result = None
    while True:
        try:
            watchdog = Watchdog(timeout)
            result = f(*args, **kwargs)
            watchdog.stop()
            break
        except Watchdog:
            continue
        except Exception as exception:
            log.info(str(exception))
    return result

def random_sleep(interval: str):
    seconds = randint(*[int(i) for i in interval.split('-')])
    log.info(f'Sleep for {seconds} seconds...')
    sleep(seconds)

def time_in_range(interval: str):
    t1, t2 = interval.split('-')
    now_time = datetime.datetime.now().time()
    start = datetime.datetime.strptime(t1, '%H:%M').time()
    end = datetime.datetime.strptime(t2, '%H:%M').time()
    result = start <= now_time or now_time <= end
    if start <= end:
        result = start <= now_time <= end
    return result