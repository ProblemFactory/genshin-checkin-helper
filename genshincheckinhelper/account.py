import re
import config
import setting
from request import http
from loghelper import log
from error import CookieError

headers = setting.headers.copy()
headers.pop("DS")
headers.pop("Origin")
headers.pop("Referer")

def login(account_data):
    if account_data["cookie"] == '':
        log.error("请填入Cookies!")
        raise CookieError('No cookie')
    # 判断Cookie里面是否有login_ticket 没有的话直接退了
    if account_data.get('login_ticket',"")=="" and "login_ticket" in account_data["cookie"]:
        temp_cookies = account_data["cookie"].split(";")
        for i in temp_cookies:
            if i.split("=")[0].strip() == "login_ticket":
                account_data["login_ticket"] = i.split("=")[1].strip()
                break
    elif account_data.get('login_ticket',"")=="":
        log.error("cookie中没有'login_ticket'字段,请重新登录米游社，重新抓取cookie!")
        raise CookieError('Cookie lost login_ticket')
    # 这里获取Stuid，但是实际是可以直接拿cookie里面的Uid
    data = http.get(url=setting.bbs_Cookie_url.format(account_data["login_ticket"])).json()
    if "成功" in data["data"]["msg"]:
        account_data["stuid"] = str(data["data"]["cookie_info"]["account_id"])
        data = http.get(url=setting.bbs_Cookie_url2.format(
            account_data["login_ticket"], account_data["stuid"])).json()
        account_data["stoken"] = data["data"]["list"][0]["token"]
        log.info("登录成功！")
        log.info("正在保存Config！")
        config.save_config()
    else:
        log.error("cookie已失效,请重新登录米游社抓取cookie, {}".format(str(data)))
        raise CookieError('Cookie expires')

def stop_module(game_id: str) -> None:
    if game_id == "bh2_cn":
        config.config["games"]["cn"]["hokai2"]["auto_checkin"] = False
    elif game_id == "bh3_cn":
        config.config["games"]["cn"]["honkai3rd"]["auto_checkin"] = False
    elif game_id == "nxx_cn":
        config.config["games"]["cn"]["tears_of_themis"]["auto_checkin"] = False
    elif game_id == "hk4e_cn":
        config.config["games"]["cn"]["genshin"]["auto_checkin"] = False
    else:
        raise NameError
    config.save_config()


def get_account_list(game_id: str, headers: dict, raw: bool = False) -> list:
    log.info(f"正在获取米哈游账号绑定的{setting.game_id2name.get(game_id,game_id)}账号列表...")
    temp_list = []
    req = http.get(setting.account_Info_url + game_id, headers=headers)
    data = req.json()
    if data["retcode"] != 0:
        log.warning(f"获取{setting.game_id2name.get(game_id,game_id)}账号列表失败！")
        stop_module(game_id)
        raise CookieError("BBS Cookie Error: "+str(data))
    for i in data["data"]["list"]:
        temp_list.append([i["nickname"], i["game_uid"], i["region"]])
    log.info(f"已获取到{len(temp_list)}个{setting.game_id2name.get(game_id,game_id)}账号信息")
    if raw:
        return data["data"]["list"]
    return temp_list

def get_cookie_token_by_stoken(account_data):
    if account_data["stoken"] == "" and account_data["stuid"] == "":
        log.error("Stoken和Suid为空，无法自动更新CookieToken")
        raise CookieError('Cookie expires')
    data = http.get(url=setting.bbs_get_cookie_token_by_stoken,
                    params={"stoken": account_data["stoken"], "uid": account_data["stuid"]},
                    headers=headers).json()
    if data.get("retcode", -1) != 0:
        log.error("stoken已失效，请重新抓取cookie")
        raise CookieError('Cookie expires')
    return data["data"]["cookie_token"]

def update_cookie_token(account_data) -> bool:
    log.info("CookieToken失效，尝试刷新")
    old_token_match = re.search(r'cookie_token=(.*?)(?:;|$)', account_data["cookie"])
    if old_token_match:
        new_token = get_cookie_token_by_stoken(account_data)
        log.info("CookieToken刷新成功")
        account_data["cookie"] = account_data["cookie"].replace(
            old_token_match.group(1), new_token)
        config.save_config()
        return True
    return False